//
//  ViewController.m
//  VucutKitleIndeksi
//
//  Created by Yakup on 23.07.2017.
//  Copyright © 2017 Yakup. All rights reserved.
//

#import "ViewController.h"
#import "DetaylarViewController.h"

@interface ViewController ()

@end

@implementation ViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    
  // detaylar = [[DetaylarV alloc] initWithNibName:@"DetaylarV" bundle:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}







double vki;
@synthesize Ad,Durum;

-(float)VkiHesapla:(float)boy ve:(float)kilo{
    float sonuc = (kilo/(boy*boy));
    return sonuc;
}



- (IBAction)sonucBtn:(id)sender {
    float boy = [_Boy.text floatValue] / 100;
    float kilo = [_Kilo.text floatValue];
   
    NSString *name = [Ad text];
    NSString *mrb = @"Merhaba ";
    NSString *mrb1;
    mrb1 = [[NSString alloc] initWithFormat:@"%@ %@",mrb,name];
    _Merhaba.text = mrb1;
    
    if(_Boy.text.length > 0 || _Kilo.text.length > 0){
    
        if(_switch1.on){
           //Kadın
            vki = [self VkiHesapla:boy ve:kilo];
            
            if(vki<=16)                     [Durum setText:@"Zayıf"];
            else if(vki>16 && vki<=22)      [Durum setText:@"Normal"];
            else if(vki>22 && vki<= 26.5)   [Durum setText:@"Kilolu"];
            else if(vki>26.5)               [Durum setText:@"Aşırı Kilolu"];
            
            
            _Sonuc.text = [NSString stringWithFormat:@"%.2f",vki];
            //[self.view endEditing:YES];
        }
    
        else{
            //Erkek
            vki = [self VkiHesapla:boy ve:kilo];
            
            if(vki<=18.5)                   [Durum setText:@"Zayıf"];
            else if(vki>18.5 && vki<=22.5)  [Durum setText:@"Normal"];
            else if(vki>22.5 && vki <=27.5) [Durum setText:@"Kilolu"];
            else if(vki>27.5)               [Durum setText:@"Aşırı Kilolu"];

            
            
            _Sonuc.text = [NSString stringWithFormat:@"%.2f",vki];
            [self.view endEditing:YES];
            }

    }//Boşmu kontrolü if bitişi.
    else{
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Boş Bırakılamaz"
                              message:@"Bilgilerinizi Giriniz"
                              delegate:self
                              cancelButtonTitle:@"İptal"
                              otherButtonTitles:@"Tamam",nil];
                              [alert show];
        
        }
   /*_Boy.text = @"";
    _Kilo.text = @"";
     Ad.text = @"";
   */
}

//View'ler arası veri aktarımı.
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{

    DetaylarViewController *detaylarView;
    detaylarView = [segue destinationViewController];
    
    detaylarView.strgelenAd = Ad.text;
    detaylarView.dblgelenSonuc = vki;
    detaylarView.strgelenDurum = Durum.text;
    
}


@end
