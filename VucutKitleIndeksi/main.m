//
//  main.m
//  VucutKitleIndeksi
//
//  Created by Yakup on 23.07.2017.
//  Copyright © 2017 Yakup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
