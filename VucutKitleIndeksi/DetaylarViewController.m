//
//  DetaylarViewController.m
//  VucutKitleIndeksi
//
//  Created by Yakup on 25.07.2017.
//  Copyright © 2017 Yakup. All rights reserved.
//

#import "DetaylarViewController.h"

@interface DetaylarViewController ()

@end

@implementation DetaylarViewController

@synthesize strgelenAd,dblgelenSonuc,gelenAd,gelenSonuc,strgelenDurum;

- (void)viewDidLoad {
    [super viewDidLoad];
    
        gelenAd.text = strgelenAd;
        // Gelen sonucu double olduğu için cast işlemi.
        gelenSonuc.text = [NSString stringWithFormat:@"%.2f",dblgelenSonuc];
    

    
        if([strgelenDurum isEqualToString:@"Zayıf"]==1){
            [_oneri1Tv setText:@"Öğünlerinizi Atlamamalısınız. Besleyici yiyecekleri yemeli ve Bir Diyetisyene görünmelisiniz..."];
            [_oneri2Tv setText:@"Karbonhidrat bakımından zengin yiyecekler tüketmeye özen gösterin."];
        }
        else if([strgelenDurum isEqualToString:@"Normal"]==1){
            [_oneri1Tv setText:@"İdeal sayılabilecek vucut kitle indeksine sahipsiniz formunuzu korumak için  spor yapmayı ihmal etmeyin."];
            [_oneri2Tv setText:@"Protein ağırlıklı besinleri, öğünlerinizde ağırlıklı olarak bulundurun."];
        }
        else if([strgelenDurum isEqualToString:@"Kilolu"]==1){
            [_oneri1Tv setText:@"Sağlıklı ve fit bir vucut için karbonhidrat tüketimini önemli derece de azaltmananız gerekiyor."];
            [_oneri2Tv setText:@"Bir Diyetisyene başvurarak, gerekli desteği alıp spor yapmaya başlayın."];
        }
        else if ([strgelenDurum isEqualToString:@"Aşırı Kilolu"]==1){
            [_oneri1Tv setText:@"adwa."];
            [_oneri2Tv setText:@".dwad"];
        }

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}




@end
