//
//  ViewController.h
//  VucutKitleIndeksi
//
//  Created by Yakup on 23.07.2017.
//  Copyright © 2017 Yakup. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *Ad;
@property (weak, nonatomic) IBOutlet UITextField *Boy;
@property (weak, nonatomic) IBOutlet UITextField *Kilo;
@property (weak, nonatomic) IBOutlet UILabel *Sonuc;
@property (weak, nonatomic) IBOutlet UILabel *Durum;
@property (weak, nonatomic) IBOutlet UILabel *Merhaba;
@property (weak, nonatomic) IBOutlet UISwitch *switch1;


-(IBAction)sonucBtn:(id)sender;
-(float)VkiHesapla:(float)boy ve:(float)kilo;



@end

