//
//  AppDelegate.h
//  VucutKitleIndeksi
//
//  Created by Yakup on 23.07.2017.
//  Copyright © 2017 Yakup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

